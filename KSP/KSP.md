Portmod's support for KSP builds on top of the existing and actively maintained [CKAN-meta](https://github.com/KSP-CKAN/CKAN-meta) package repository.
Additional packages can be found in the [ksp](https://gitlab.com/portmod/ksp) repository. 

- https://gitlab.com/portmod/ksp
- https://gitlab.com/portmod/ckan

## Setup

KSP supports the different KSP versions via different architectures. Currently, only minor versions are specified, and the patch versions are grouped with their minor version.

You can create a KSP prefix using the command: `portmod init <prefix-name> ksp-<ver> /path/to/ksp/install`. This will create the prefix inside of the existing KSP installation. Note that no verification is done for the given path. It must be the root of the KSP installation, that is, the directory containing the executable. If you install into the wrong directory, you can remove the prefix with `portmod <prefix-name> destroy`, and start again. 

E.g. `portmod init ksp ksp-1.12 "~/.local/share/Steam/steamapps/common/Kerbal Space Program"`.

Portmod will install documentation into `doc` (though this is limited to autodetected files as CKAN does not specify bundled documentation), and generated files such as the package database will be found in `var`. 

Note that, as the `ckan` repository is very large, the first synchronization will be slow while the cache is populated (as well as the first time after each update to portmod itself). 

### Repository Selection

The interactive `init` subcommand may prompt you to select repositories if the ksp repository is already in [repos.cfg](Configuration/Repos.cfg). Make sure to select the ckan repository otherwise very few packages will be available.

### Profile Selection

Profiles are defined by the repository, and provide default settings for a prefix.

There is currently only one profile available in the ksp repository, and you will be prompted to select it during the `init` process.

## Migrating KSP versions

No support is provided at the moment for migrating from one version of KSP to another, however it should be possible to edit the prefix file (`~/.local/share/portmod/prefix` on Linux) to change the architecture, and then perform a world update to update packages to work with the new version. 

This does not currently work for downgrading the KSP version. 

Also note that packages which don't have a version that supports the new version of KSP will not be upgraded and will need to be removed manually. 
