Significant amounts of information which used to be found here can now be found at https://portmod.readthedocs.io/en/stable/index.html. General information about portmod can be found there, while specific information about game engines remains on this wiki.

To modify this wiki, you can make a merge request to https://gitlab.com/portmod/portmod-wiki. Contributions via merge request are always welcome. If you've created a package repository and would like it added to the official list, create a merge request on the [meta](https://gitlab.com/portmod/meta) repository and add it to `metadata/repos.cfg`.

Details about specific architectures can be found below, some of which have dedicated subcategories.

## OpenMW (openmw)

See [OpenMW](OpenMW/OpenMW)

Original and largest package repository for portmod.

https://gitlab.com/portmod/openmw-mods.git

## GZDoom (gzdoom)

https://gitlab.com/portmod/zdoom.git

## X3: Albion Prelude (x3ap)

https://gitlab.com/portmod/x3.git

## TES3MP (tes3mp)

Support for server-side scripts is available via the tes3mp-server repository: https://gitlab.com/portmod/tes3mp-server.git

Client-side mods can be installed through the openmw repository (https://gitlab.com/portmod/openmw-mods.git). Note that while a tes3mp architecture also exists in that repository, it is mostly untested. In general, packages in the `assets-*` categories *should* be functional (though few if any have been marked as stable on `tes3mp`).

## Bethesda Games

These games are grouped together since they are handled almost identically. They have not received very much attention, and need a bit of polish, so they should be considered **unstable**, however they do generally seem to work.

- **The Elder Scrolls IV: Oblivion (oblivion)**: https://gitlab.com/portmod/oblivion.git
- **Fallout: New Vegas (fallout-nv)**: https://gitlab.com/portmod/fallout-nv.git
- **Fallout 4 (fallout-4)**: https://gitlab.com/portmod/fallout-4.git

See [Bethesda](Bethesda/Bethesda) for setup details.

## Kerbal Space Program (ksp)

See [KSP](KSP/KSP).

- https://gitlab.com/portmod/ksp
- https://gitlab.com/portmod/ckan

## Contributing

[Mod Maintainers](Mod-Maintainers)

[Guide to Websites Hosting Mods](Guide to Websites Hosting Mods)

## Usage Notes

[Windows Issues](Windows)

- In general, manual configuration is not recommended when using Portmod as it may lead to undesired behaviour, notably, the potential loss of your changes when running updates. If there is a configuration step that a pybuild omits you have the following options:
    1. Report the issue and wait for a fix
    2. Fix the pybuild yourself:
        - Create a local repository and add it to repos.cfg in the config directory
        - Copy and modify the offending pybuild.
        - Once you've added and tested the fix, please submit the changes to the repository so that others can benefit from the change.
