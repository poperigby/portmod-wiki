- [Homepage](https://gitlab.com/bmwinger/tr-patcher)
- [Package Information](https://portmod.gitlab.io/openmw-mods/bin/tr-patcher/)

### Unable to locate Java Runtime on macOS

See #325

If tr-patcher is unable to locate the java runtime, it may be necessary to explicitly set `JAVA_HOME` to point to your java installation. This should be able to be set in `portmod.conf`.

E.g.

From https://gitlab.com/portmod/portmod/-/issues/325#note_745884833

```bash
export JAVA_HOME=$(/usr/libexec/java_home -v 1.8.0_302)
```
Note that the version passed probably will vary depending on the version of java you have installed