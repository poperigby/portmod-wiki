- [Official Homepage](https://imagemagick.org/)
- [GitHub](https://github.com/ImageMagick/ImageMagick)
- [Wikipedia](https://en.wikipedia.org/wiki/ImageMagick)
- [Package Information](https://portmod.gitlab.io/openmw-mods/sys-bin/magick/) ([binary](https://portmod.gitlab.io/openmw-mods/bin/imagemagick/))

Two ImageMagick packages are provided by portmod:
- [sys-bin/magick](https://portmod.gitlab.io/openmw-mods/sys-bin/magick/): Which is just for detecting if you have imagemagick installed either through portmod or on your host system.
- [bin/imagemagick](https://portmod.gitlab.io/openmw-mods/bin/imagemagick/): Which installs the ImageMagick official AppImage with a workaround to allow it to run when sandboxed.

## Frequently Encountered Issues

### Security Policy

ImageMagick has [optional security policies](https://imagemagick.org/script/security-policy.php) which may restrict its use when processing extremely large textures. E.g. see [Project Atlas](./Project-Atlas).

### magick executable not found

ImageMagick 6 has long since been succeeded by ImageMagick 7. If your OS/distribution does not provide ImageMagick 7 in its package repositories, you can instead install [bin/imagemagick](https://portmod.gitlab.io/openmw-mods/bin/imagemagick/) through portmod.

### ImageMagick AppImage does not work

You should not use the AppImage version of ImageMagick, as it is incompatible with bubblewrap and will not run in the sandbox.
You can instead use the [bin/imagemagick](https://portmod.gitlab.io/openmw-mods/bin/imagemagick/) package, which is based on the extracted AppImage version (since it's AppImage's use of overlayfs when running the image file which is incompatible with bubblewrap).
