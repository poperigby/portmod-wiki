Original and most active package repository for portmod.

https://gitlab.com/portmod/openmw-mods.git

- [Setup Guide](OpenMW/Setup): Creating and setting up a prefix for OpenMW
- [WSL Setup Guide](OpenMW/WSL Setup): Creating and setting up a prefix for OpenMW on WSL.
- [Contributing Guide](https://gitlab.com/portmod/openmw-mods/-/blob/master/CONTRIBUTING.md): How to contribute to the package repositories
- [Local Mods](OpenMW/Local-Mods): Installing mods manually alongside portmod packages
- [Prefix Layout](OpenMW/Prefix-Layout): Physical layout of openmw prefixes
- [True Type Fonts](OpenMW/true-type-fonts): Enabling true type fonts
- [User Sorting Rules](OpenMW/user-sorting-rules)
- [Guide to creating packages](OpenMW/Package-Guide)
- [Packaging Guidelines](OpenMW/Packaging-Guidelines)
- [Normal Maps](OpenMW/Normal-Maps)

#### bsatool
Portmod's support for reading bsas depends on `bsatool` (for reading the VFS archives), which is usually packaged with OpenMW.

The executable must be in your `PATH` so that portmod can find it, though on Windows it can also be found via the `HKEY_LOCAL_MACHINE\Software\Wow6432Node\OpenMW.org` registry key.

On certain platforms it may be optional:
- Gentoo: Available with `games-engines/openmw[devtools]`

## Packages with their own Wiki Pages

- [ImageMagick](./pkg/ImageMagick)
- [Project Atlas](./pkg/Project-Atlas)
- [TR Patcher](./pkg/TR-Patcher)