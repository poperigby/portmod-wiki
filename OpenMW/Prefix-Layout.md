Note: All file paths are specified relative to the prefix root.

The following directories should be present in an OpenMW prefix:

```
bin/
etc/
doc/
lib/
modules/
pkg/
share/
var/
```

## Configuration

`etc/configtool` contains configtool configuration files for each mod installed. These set up the VFS and detail ordering relationships between data directories, plugins, bsa files, groundcover files, etc.

## Mod Data

"Normal" mod data is installed into `pkg/{CATEGORY}/{PN}`. These directories are then included in portmod's internal representation of the OpenMW VFS, and added to `openmw.cfg` by `modules/openmw-config`.

Documentation is installed into `doc/{PN}`

## Tools and Libraries

Binaries are installed into `bin`.

Libraries, such as for python executables, are installed into `lib`.

Shared data, such as for fonts, is installed into `share`.

### Python

`lib/python` is used to store python libraries which are version-independent. Currently, python libraries which have different installations depending on python version (e.g. due to a compiled extension) are not supported. See `common/distutils` for information on how python packages are installed.

### Generated Data

Generated data should be stored in `var`. Packages are forbidden from installing directly into `var` for this reason. `var` also contains information about the prefix such as `var/db`, the package database.

### Portmod modules

Portmod [modules](../Modules) are installed into `modules`. These may depend on python libraries installed into `lib/python`, but should otherwise be contained in a single file, containing the special functions described on the [Modules](../Modules) page.
