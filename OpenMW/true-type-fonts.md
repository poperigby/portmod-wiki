True Type Fonts can be configured using the `modules/openmw-fonts` package. This will automatically generate the `openmw_font.xml` file (briefly mentioned in the [openmw documentation](https://openmw.readthedocs.io/en/stable/reference/modding/font.html)) using fonts you select through the interface.

Three font types are used by openmw:

- `daedric` - Used for displaying daedric text.
- `magic` - Used for most interface text.
- `mono` - Used for the console.

Configuration in general is done using the `portmod <prefix> select fonts` interface. You can display available fonts using the command:

```
portmod <prefix> select fonts list {type}
```

where `type` is one of the types listed above, and set fonts using the command:

```
portmod <prefix> select fonts set {type} {value}
```

where value is either the name of the font, or its index in the list.

Fonts from the `media-fonts` category in the repository will appear in the list, as well as system fonts on systems with [Fontconfig](https://www.freedesktop.org/wiki/Software/fontconfig/) (except for Daedric, which is assumed will only come from the repo). Note that fonts are filtered by type to avoid displaying too many unnecessary fonts.

## OpenMW 0.48 pre-releases/development version

Note that it's been reported that the latest development version of OpenMW no longer supports this method of enabling fonts. Since these are not stable releases, this information is subject to change at any time (please update this section if it does).

You can enable the new built-in True Type fonts by following [the OpenMW Font documentation](https://openmw.readthedocs.io/en/latest/reference/modding/font.html), noting that you should instead add the config values to `fallback.cfg` (see [User Sorting Rules](./User-Sorting-Rules)).

Fonts in the repository do not currently provide the necessary `.omwfont` files to be detectable by the new system, however this format seems to be the same as the existing `openmw_font.xml` used by the font module.