In addition to OpenMW-style normal maps (see [OpenMW Docs](https://openmw.readthedocs.io/en/stable/reference/modding/settings/shaders.html#auto-use-object-normal-maps)), portmod supports MGE-style normal maps via two methods.

The default method is to install the maps as-is, while enabling the 
[apply lighting to environment maps](https://openmw.readthedocs.io/en/stable/reference/modding/settings/shaders.html#apply-lighting-to-environment-maps) 
setting. and is supported by OpenMW since version 0.46.

The second and older method is "fixing" via openmw-nif-cleaner, which can be enabled on specific packages via the [fixmaps](https://portmod.gitlab.io/openmw-mods/flags/fixmaps/) use flag. 

The MGE-style bump maps are a little more bumpy, but also slightly shiny.
There also seems to be a difference in light levels, making the "fixed" maps
make indoor environments a little darker by comparison.

The default is set to the new method since it's the simpler option, closer to
the original, and probably has more appropriate light levels (particularly if
combined with a mod that adjusts indoor ambient light
like [true-lights-and-darkness](https://portmod.gitlab.io/openmw-mods/gameplay-misc/true-lights-and-darkness/).
