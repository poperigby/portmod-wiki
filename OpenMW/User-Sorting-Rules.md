User config rules can be specified via a csv file in `PORTMOD_CONFIG_DIR/config/TYPE.csv`, where type is the name of the type of element being sorted (defined in the profile). These include `install`, `plugins`, `archives`, etc. See [Portmod Config](portmod config) for the location of `PORTMOD_CONFIG_DIR`.

## Purpose
User config rules are designed to handle mods where the choice of which is loaded first is subjective, such as choosing the ordering of two texture mods that have some overlap in which textures they implement.

It can also be used to fix mods that are loaded out of order, but *please notify the devs, either via the forums, or gitlab, if there is a missing sorting rule that all users would need so that the change can be added to the tree!*.

## Usage

For the most part, only the following files should need to be edited.
- `config/plugins.csv` - Plugin sorting rules. Should contain the names of the plugins.
- `config/install.csv` - Data directory sorting rules. Should contain the Atoms of the mods.
- `config/settings.cfg` - Custom settings to be applied to openmw's settings.cfg. Unlike its handling of `openmw.cfg`, openmw-config will preserve manual changes to `settings.cfg`, however this can be used in case you want to override a setting which portmod adds.
- `config/fallback.cfg` - `fallback=` values for `openmw.cfg`. These should be specified in `ini` form. 

The first column in the csv file is the overriding entry, with all other columns being the overridden entries (I.e., you specify a one to many relationship in each line of the file, with the one overriding entry being the entry that ends up loading last). Quotation marks can be used to handle file names that contain commas.

E.g. File `config/install.csv`
```csv
assets-textures/morrowind-vanillaplus-textures, assets-textures/morrowind-enhanced-textures
assets-textures/full-dwemer-retexture, assets-textures/morrowind-vanillaplus-textures
```

E.g. File `config/plugins.csv`
```csv
Tribunal.esm, Morrowind.esm
Bloodmoon.esm, Tribunal.esm
"Badly, named plugin.esp", Morrowind.esm
```

The `cfg` files listed above should be in `ini` form. For `settings.cfg`, this is the same as OpenMW's `settings.cfg`. For `fallback.cfg`, they should be specified as they would have been in `Morrowind.ini` for the vanilla engine.

E.g. `fallback=Blood_Model_0,BloodSplat.nif` would be specified as 
```
[Blood]
Model 0=BloodSplat.nif
```
Note that spaces can be considered equivalent to underscores in keys (but not in values), and either can be used here.