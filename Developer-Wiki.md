This is a place to document information developers may need to know about things which aren't suitable anywhere else.

## Release Announcements:

Release Announcements should be made in the following locations:

### Every Release:
- [~bmw/portmod-announce@lists.sr.ht](mailto:~bmw/portmod-announce@lists.sr.ht)

### Every Major/Minor Release:
This has not been done in the past, but should help advertise portmod

#### Reddit
(format favours new posts for each release, but old release posts are shown here as examples):

- r/tes3mods: https://reddit.com/r/tes3mods/comments/11abqxv/portmod_modpackage_manager_257/?
- r/Morrowind: https://reddit.com/r/Morrowind/comments/11ac6yq/portmod_modpackage_manager_257/?

<details><summary>Template (OpenMW/Morrowind)</summary>

```markdown
Homepage: https://gitlab.com/portmod/portmod

Wiki: https://gitlab.com/portmod/portmod/-/wikis/OpenMW/OpenMW (OpenMW-specific info and guides)

Documentation: https://portmod.readthedocs.org/ (specific to the portmod tool itself)

Release page: https://gitlab.com/portmod/portmod/-/releases/v2.5.7 (for installation information, see [the guide](https://portmod.readthedocs.io/en/stable/install/index.html) on the docs)

Portmod is a package manager for Linux, macOS and Windows, targeting game mods. It's been under development since the beginning of 2019, primarily targeting OpenMW, but now also provides support for a few other games.

It's currently CLI-only, though work on a GUI is in-progress.

## What Portmod Does

To try and describe it in a way that distinguishes it from other mod managers available for Morrowind, Portmod supports fully automated installation of individual mods and their dependencies, including automatically patching and configuring them to work with other mods already installed.
This is accomplished through package files which describe the structure of the mod in detail so that portmod knows how to install and configure it, though the downside is that these package files require continual updating as new versions of mods are released.
Packages may need to be fetched manually depending on their source, as direct download links are not always available.
The general idea is that while mod installation is still just as complicated as manual installation through another mod manager would be, with portmod it only needs to be done once, documented in a package file, and shared with the community, after which point installation becomes reproducible by anyone else by simply running a single command in a terminal.

As there are far more mods in existence than could possibly be packaged by the developers of portmod, portmod relies on community contributions from users to keep the mod package repositories accurate, up to date and relevant.

## Original Morrowind Engine

Portmod does not currently support the original morrowind engine, largely since there has been little interest expressed, though admittedly I've never really advertised portmod outside of the OpenMW community in the past. Support may be relatively straightforward (assuming we can set up a BSA vfs like what was used for Oblivion/Fallout 4/NV, otherwise it's more complicated), and many mods already packaged for OpenMW could work without changes. The main thing lacking, assuming that there is sufficient interest, is that we would need volunteers to test packages and mark them as stable on morrowind (as well as contributing packages for any morrowind-only mods which are wanted).
```

</details>

#### Kerbal Space Program Forums:
https://forum.kerbalspaceprogram.com/index.php?/topic/212258-portmod-package-manager-v257/

#### OpenMW Forums:
https://forum.openmw.org/viewtopic.php?f=40&t=5875

#### Nexusmods Forums
https://forums.nexusmods.com/index.php?/topic/12658992-portmod-modpackage-manager/
